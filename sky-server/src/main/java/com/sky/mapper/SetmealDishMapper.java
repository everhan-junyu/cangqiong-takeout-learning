package com.sky.mapper;// 直接赋值粘贴，删除CSDN的权限转载中文

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @program: sky-take-out
 * @description:
 * @author: Mr.han
 * @create: 2024-01-08 18:19
 **/
@Mapper
public interface SetmealDishMapper {
        /*根据菜品id查询套餐id
        * @param dishId
        * @return
        * */
        List<Long> getSetmealIdsByDishIds(List<Long> dishIds);


}
